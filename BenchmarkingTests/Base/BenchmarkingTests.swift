//
//  BenchmarkingTests.swift
//  BenchmarkingTests
//
//  Created by Tim Fuqua on 12/27/21.
//  Copyright © Norswedgian Studios. All rights reserved.
//

import XCTest
@testable import Benchmarking

class BenchmarkTests: XCTestCase {
    enum Defaults {
        fileprivate static let stressTest = StressTest()
    }
    
    var stressTester: StressTest { Defaults.stressTest }
}
