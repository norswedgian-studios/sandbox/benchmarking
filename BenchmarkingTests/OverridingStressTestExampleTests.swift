//
//  OverridingStressTestExampleTests.swift
//  BenchmarkingTests
//
//  Created by Tim Fuqua on 12/27/21.
//  Copyright © Norswedgian Studios. All rights reserved.
//

import XCTest
@testable import Benchmarking

class OverridingStressTestExampleTests: BenchmarkTests {
    override var stressTester: StressTest { StressTest(iterations: 20000, runs: 4) }
}

extension OverridingStressTestExampleTests {
    func test_firstFormat() {
        print(
            stressTester.run {
                let _: CGFloat = 10
            }
        )
    }
    
    func test_secondFormat() {
        print(
            stressTester.run {
                let _ = CGFloat(10)
            }
        )
    }
    
    func test_thirdFormat() {
        print(
            stressTester.run {
                let _ = 10 as CGFloat
            }
        )
    }
}
