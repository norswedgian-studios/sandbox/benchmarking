//
//  ContentView.swift
//  Benchmarking
//
//  Created by Tim Fuqua on 12/27/21.
//  Copyright © Norswedgian Studios. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
